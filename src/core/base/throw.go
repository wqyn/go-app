package base

import (
	"fmt"
	"runtime"
	"strings"
)

/**
 * 错误信息
 * @return string
 */
func Msg() string {

	var throwStack = []string{}

	for i := 0; ; i++ {
		if _, file, no, ok := runtime.Caller(i); true == ok {
			throwStack = append(throwStack, fmt.Sprintf("%d %s at %d", i, file, no))
		} else {
			break
		}
	}

	return strings.Join(throwStack, "\r\n")
}
