package routing

/**
 * MIME类型
 */
var mimes = map[string]string{}

/**
 * 返回支持的MIME类型
 * @return map[string]string
 */
func Mimes() map[string]string {
	return mimes
}
