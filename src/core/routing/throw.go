package routing

import (
	"fmt"
	"runtime"
	"strings"
)

/**
 * 无效路由动词
 * @param verb string
 */
func throwInvalidVerb(verb string) {
	panic("routing: invalid verb " + verb + "\r\n" + errStr())
}

/**
 * 错误字符串
 * @return string
 */
func errStr() string {

	var throwStack = []string{}

	for i := 0; ; i++ {
		if _, file, no, ok := runtime.Caller(i); true == ok {
			throwStack = append(throwStack, fmt.Sprintf("%d %s at %d", i, file, no))
		} else {
			break
		}
	}

	return strings.Join(throwStack, "\r\n")
}
