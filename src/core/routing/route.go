package routing

/**
 * 路由单元
 */
type Route struct {
	path    string
	action  Action
	alias   string
	host    []string
	port    []string
	ext     []string
	param   map[string]string
	pattern map[string]string
	filter  map[string][]FilterFunc
}

/**
 * 注册路由别名
 * @param alias string
 * @return *Route
 */
func (r *Route) Alias(alias string) *Route {
	r.alias = alias
	return r
}

/**
 * 注册匹配模式
 * @param name  string
 * @param value string
 * @return *Route
 */
func (r *Route) Pattern(name string, value string) *Route {

	if nil == r.pattern {
		r.pattern = make(map[string]string)
	}

	r.pattern[name] = value

	return r
}

/**
 * 注册隐式参数
 * @param name string
 * @param value string
 * @return *Route
 */
func (r *Route) Param(name string, value string) *Route {

	if nil == r.param {
		r.param = make(map[string]string)
	}

	r.param[name] = value

	return r
}

/**
 * 绑定主机
 * @param host ... string
 * @return *Route
 */
func (r *Route) Host(hosts ...string) *Route {

	if nil == r.host {
		r.host = []string{}
	}

	for _, host := range hosts {
		r.host = append(r.host, host)
	}

	return r
}

/**
 * 绑定端口
 * @param ports ... string
 * @return *Route
 */
func (r *Route) Port(ports ...string) *Route {

	if nil == r.port {
		r.port = []string{}
	}

	for _, port := range ports {
		r.port = append(r.port, port)
	}

	return r
}

/**
 * 注册后缀
 * @param exts ... string
 * @return *Route
 */
func (r *Route) Ext(exts ...string) *Route {

	if nil == r.ext {
		r.ext = []string{}
	}

	for _, _ext := range exts {
		r.ext = append(r.ext, _ext)
	}

	return r
}

/**
 * 注册前置过滤器
 * @param filter FilterFunc
 * @return *Route
 */
func (r *Route) Before(filter FilterFunc) *Route {

	if nil == r.filter {
		r.filter = make(map[string][]FilterFunc)
	}

	if nil == r.filter["before"] {
		r.filter["before"] = []FilterFunc{}
	}

	r.filter["before"] = append(r.filter["before"], filter)

	return r
}

/**
 * 注册后置过滤器
 * @param filter FilterFunc
 * @return *Route
 */
func (r *Route) After(filter FilterFunc) *Route {

	if nil == r.filter {
		r.filter = make(map[string][]FilterFunc)
	}

	if nil == r.filter["after"] {
		r.filter["after"] = []FilterFunc{}
	}

	r.filter["after"] = append(r.filter["after"], filter)

	return r
}

/**
 * 返回路由Before过滤器
 * @return map[string]FilterFunc
 */
func (r *Route) GetBefore() []FilterFunc {
	return r.filter["before"]
}

/**
 * 返回路由After过滤器
 * @return map[string]FilterFunc
 */
func (r *Route) GetAfter() []FilterFunc {
	return r.filter["after"]
}

/**
 * 返回路由匹配模式
 * @return map[string]string
 */
func (r *Route) GetPattern() map[string]string {
	return r.pattern
}

/**
 * 返回路由隐式参数
 * @return map[string]string
 */
func (r *Route) GetParam() map[string]string {
	return r.param
}

/**
 * 返回路由PATH
 * @return string
 */
func (r *Route) GetPath() string {
	return r.path
}

/**
 * 返回路由动作
 * @return Action
 */
func (r *Route) GetAction() Action {
	return r.action
}

/**
 * 返回绑定域名
 * @return []string
 */
func (r *Route) GetHost() []string {
	return r.host
}

/**
 * 返回绑定端口
 * @return []string
 */
func (r *Route) GetPort() []string {
	return r.port
}

/**
 * 返回后缀
 * @return []string
 */
func (r *Route) GetExt() []string {
	return r.ext
}
