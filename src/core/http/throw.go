package http

/**
 * 解析表单错误
 * @param err string
 */
func throwParseForm(err string) {
	panic(err)
}

/**
 * 解析查询字符串错误
 */
func throwParseQueryString(err string) {
	panic(err)
}
