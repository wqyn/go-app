package http

import (
	"core/base"
	"mime/multipart"
	NetHttp "net/http"
	"path"
	"strings"
	"time"
)

/**
 * 上传文件信息结构体
 */
type FileInfo struct {
	name string
	mime string
	size int64
	ext  string
	file *multipart.FileHeader
}

/**
 * 请求信息结构体
 */
type Req struct {
	req    *NetHttp.Request
	time   int64
	get    Value
	data   Value
	post   Value
	file   Value
	cookie Value
	header Value
}

/**
 * 返回请求对象
 * @param req *NetHttp.Request
 * @return *Req
 */
func Request(req *NetHttp.Request) *Req {

	parseForm(req)

	return &Req{
		req:    req,
		time:   time.Now().Unix(),
		get:    parseQuery(req.URL.RawQuery),
		post:   parseValues(req.PostForm),
		data:   parseValues(req.Form),
		file:   parseFile(req),
		cookie: parseQuery(req.Header.Get("Cookie")),
	}
}

/**
 * 返回主机地址
 * @return string
 */
func (req Req) Host() string {
	return req.req.Host
}

/**
 * 返回请求端口
 * @return string
 */
func (req Req) Port() string {

	_v := parseHost(req.Host())

	if 1 < len(_v) {
		return _v[1]
	}

	return ""
}

/**
 * 返回域名
 * @return string
 */
func (req Req) Domain() string {
	return parseHost(req.Host())[0]
}

/**
 * 返回请求URL,包含请求参数
 * @return string
 */
func (req Req) Url() string {
	return req.req.RequestURI
}

/**
 * 返回请求URL,不包含请求参数
 * @return string
 */
func (req Req) RawUrl() string {
	return req.req.URL.Path
}

/**
 * 返回请求地址后缀
 * @return string
 */
func (req Req) Ext() string {
	return strings.TrimLeft(path.Ext(req.RawUrl()), ".")
}

/**
 * 返回查询字符串
 * @return string
 */
func (req Req) Query() string {
	return req.req.URL.RawQuery
}

/**
 * 返回HTTP协议版本
 * @return string
 */
func (req Req) Protocol() string {
	return req.req.Proto
}

/**
 * 返回用户User-Agent头
 * @return string
 */
func (req Req) UserAgent() string {
	return req.req.UserAgent()
}

/**
 * 返回内容类型
 * @return string
 */
func (req Req) ContentType() string {
	return req.Header().Get("Content-Type")
}

/**
 * 返回请求语言
 * @return string
 */
func (req Req) Lang() string {
	return req.Header().Get("Accept-Language")
}

/**
 * 返回请求头信息
 * @return NetHttp.Header
 */
func (req Req) Header() NetHttp.Header {
	return req.req.Header
}

/**
 * 返回请求IP
 * @return string
 */
func (req Req) Ip() string {

	_ip := "unknown"

	if _ip = req.Header().Get("X-Real-Ip"); "" != _ip {
		return _ip
	}

	if _ip = req.Proxy(); "" != _ip {

		if _index := strings.LastIndex(_ip, ","); 0 < _index {
			return string([]uint8(_ip)[2+_index:])
		}

		return _ip
	}

	return _ip
}

/**
 * 返回代理IP
 * @return []string
 */
func (req Req) Proxy() string {
	return req.Header().Get("X-Forwarded-For")
}

/**
 * 返回请求的来源地址
 * @return string
 */
func (req Req) Referer() string {
	return req.req.Referer()
}

/**
 * 返回当前请求时间戳
 * @return int64
 */
func (req Req) Time() int64 {
	return req.time
}

/**
 * 返回请求方法
 * @return string
 */
func (req Req) Method() string {
	return req.req.Method
}

/**
 * 返回URL的SCHEME部分
 * @return string
 */
func (req Req) Scheme() string {

	if _v := req.Header().Get("Sheme"); "" != _v {
		return _v
	}

	if true == base.App.EnableTLS {
		return "https"
	}

	return "http"
}

/**
 * 返回请求浏览器器
 * @return string
 */
func (req Req) Browser() string {
	return parseBrowser(req.UserAgent())
}

/**
 * 返回常见操作系统类型
 * @return string
 */
func (req Req) Os() string {
	return parseOs(req.UserAgent())
}

/**
* 判断是否SSL安全模式
 * @return bool
*/
func (req Req) IsSecure() bool {

	if "on" == req.Header().Get("Https") {
		return true
	}

	if "443" == req.Port() {
		return true
	}

	return false
}

/**
 * 判断是否有上传文件
 * @return bool
 */
func (req Req) IsUpload() bool {

	if nil == req.File() {
		return false
	}

	return 0 < req.File().Size()
}

/**
 * 判断是否Ajax请求
 * @return bool
 */
func (req Req) IsAjax() bool {
	return "XMLHttpRequest" == req.Header().Get("X-Requested-With")
}

/**
 * 判断是否Pjax请求
 * @return bool
 */
func (req Req) IsPjax() bool {
	return req.IsAjax() && "true" == req.Header().Get("X-Pjax")
}

/**
 * 判断是否移动端请求
 * @return bool
 */
func (req Req) IsMobile() bool {
	return isMobile(req.UserAgent())
}

/**
 * 判断请求方法
 * @param string method
 * @return bool
 */
func (req Req) Is(method string) bool {
	return strings.ToUpper(req.Method()) == strings.ToUpper(method)
}

/**
 * 返回请求指针
 * @return *NetHttp.Request
 */
func (req Req) Req() *NetHttp.Request {
	return req.req
}

/**
 * 返回请求数据,包含GET、POST等
 * @return Value
 */
func (req Req) Data() Value {
	return req.data
}

/**
 * 返回GET请求参数
 * @return Value
 */
func (req Req) Get() Value {
	return req.get
}

/**
 * 返回POST请求参数
 * @return Value
 */
func (req Req) Post() Value {
	return req.post
}

/**
 * 返回上传文件信息
 * @return Value
 */
func (req Req) File() Value {
	return req.file
}

/**
 * 返回请求Cookie
 * @return Value
 */
func (req Req) Cookie() Value {
	return req.cookie
}
