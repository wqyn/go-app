package http

import (
	"sort"
)

/**
 * 请求数据结构
 */
type Value map[string]interface{}

/**
 * 读取字典
 * @param k string
 * @return interface{}
 */
func (value Value) Get(k string) interface{} {

	if _v, ok := value[k]; ok {
		return _v
	}

	return nil
}

/**
 * 写入字典
 * @param k string
 */
func (value Value) Set(k string, v interface{}) {
	value[k] = v
}

/**
 * 删除字典
 * @param k string
 */
func (value Value) Delete(k string) {
	delete(value, k)
}

/**
 * 返回字典元素数量
 * @return int
 */
func (value Value) Size() int {
	return len(value)
}

/**
 * 判断键名是否存在
 * @param k string
 * @return bool
 */
func (value Value) Has(k string) bool {

	if _, ok := value[k]; ok {
		return true
	}

	return false
}

/**
 * 返回键名数组
 * @return []string
 */
func (value Value) Keys() []string {

	_keys := make([]string, 0)

	for _key := range value {
		_keys = append(_keys, _key)
	}

	return _keys
}

/**
 * 对结果递减排序
 * @return Value
 */
func (value Value) Desc() Value {

	_keys := value.Keys()
	sort.Stable(sort.Reverse(sort.StringSlice(_keys)))

	_data := make(Value, len(_keys))

	for _, _key := range _keys {
		_data[_key] = value[_key]
	}

	return _data
}

/**
 * 对结果递增排序
 * @return Value
 */
func (value Value) Asc() Value {

	_keys := value.Keys()
	sort.Strings(_keys)

	_data := make(Value, len(_keys))

	for _, _key := range _keys {
		_data[_key] = value[_key]
	}

	return _data
}

/**
 * 排除指定键名
 * @param keys ...string
 * @return Value
 */
func (value Value) Except(keys ...string) Value {

	_data := make(Value, len(value)-len(keys))

	for _k, _v := range value {
		if false == hasKey(_k, keys) {
			_data[_k] = _v
		}
	}

	return _data
}

/**
 * 判断键名是否存在
 * @param key string
 * @param keys []string
 * @return bool
 */
func hasKey(key string, keys []string) bool {

	for _, _k := range keys {
		if _k == key {
			return true
		}
	}

	return false
}

/**
 * 接受指定键名
 * @param keys ...string
 * @return Value
 */
func (value Value) Accept(keys ...string) Value {

	_data := make(Value, len(keys))

	for _, _k := range keys {
		if true == value.Has(_k) {
			_data[_k] = value[_k]
		}
	}

	return _data
}
