package http

import (
	NetHttp "net/http"
	"net/url"
	"path"
	"strings"
)

/**
 * 判断是否移动端
 * @param agent string
 * @return string
 */
func isMobile(agent string) bool {

	_tags := []string{"Android", "Mobile", "iPhone", "iPad"}

	for _, _tag := range _tags {
		if -1 < strings.Index(agent, _tag) {
			return true
		}
	}

	return false
}

/**
 * 判断Form提交是否是multipart/form-data
 * @param req *NetHttp.Request
 * @return bool
 */
func isMultipart(req *NetHttp.Request) bool {
	ct := req.Header.Get("Content-Type")
	return "" != ct && -1 < strings.Index(ct, "multipart/form-data")
}

/**
 * 解析表单
 * @param req *NetHttp.Request
 */
func parseForm(req *NetHttp.Request) {

	var err error

	if isMultipart(req) {
		err = req.ParseMultipartForm(100)
	} else {
		err = req.ParseForm()
	}

	if nil != err {
		throwParseForm(err.Error())
	}
}

/**
 * 解析上传的文件信息
 * @param req *NetHttp.Request
 * @return Value
 */
func parseFile(req *NetHttp.Request) Value {

	var _result = make(Value)

	if false == isMultipart(req) {
		return _result
	}

	_fileHeaders := req.MultipartForm.File

	if nil == _fileHeaders {
		return _result
	}

	for _name, _fileHeader := range _fileHeaders {
		for _, _fh := range _fileHeader {
			_result.Set(_name, FileInfo{
				_fh.Filename,
				_fh.Header.Get("Content-Type"),
				_fh.Size,
				strings.TrimLeft(path.Ext(_fh.Filename), "."),
				_fh,
			})
		}
	}

	return _result
}

/**
 * 解析查询字符串
 * @param query string
 * @return url.Values
 */
func parseQuery(qs string) Value {

	_values, _err := url.ParseQuery(qs)

	if nil != _err {
		throwParseQueryString(_err.Error())
	}

	return parseValues(_values)
}

/**
 * 解析表单值
 * @param values url.Values
 * @return Value
 */
func parseValues(values url.Values) Value {

	_result := make(Value)

	if nil != values {

		for _k := range values {
			_result.Set(_k, values.Get(_k))
		}
	}

	return _result
}

/**
 * 解析域名和端口
 * @param host string
 * @return []string
 */
func parseHost(host string) []string {
	return strings.SplitN(host, ":", 2)
}

/**
 * 解析请求浏览器
 * @param agent string
 * @return string
 */
func parseBrowser(agent string) string {

	if -1 < strings.Index(agent, "MSIE") {

		browser := "IE"

		if -1 < strings.Index(agent, "Trident") {
			browser += " 11"
		} else if -1 < strings.Index(agent, "10.0") {
			browser += " 10"
		} else if -1 < strings.Index(agent, "9.0") {
			browser += " 9"
		} else if -1 < strings.Index(agent, "8.0") {
			browser += " 8"
		} else if -1 < strings.Index(agent, "7.0") {
			browser += " 7"
		} else if -1 < strings.Index(agent, "6.0") {
			browser += " 6"
		}

		return browser
	}

	if -1 < strings.Index(agent, "Edge") {
		return "Edge"
	}

	if -1 < strings.Index(agent, "Firefox") {
		return "Firefox"
	}

	if -1 < strings.Index(agent, "Chrome") {
		return "Chrome"
	}

	if -1 < strings.Index(agent, "Safari") {
		return "Safari"
	}

	if -1 < strings.Index(agent, "Opera") {
		return "Opera"
	}

	if -1 < strings.Index(agent, "Opera") {
		return "Opera"
	}

	return "unknown"
}

/**
 * 解析请求的操作系统
 * @param agent string
 * @return string
 */
func parseOs(agent string) string {

	if -1 < strings.Index(agent, "Windows") {

		os := "Windows"

		if -1 < strings.Index(agent, "NT 5.0") {
			os += " 2000"
		} else if -1 < strings.Index(agent, "NT 5.1") {
			os += " XP"
		} else if -1 < strings.Index(agent, "NT 5.2") {
			os += " 2003"
		} else if -1 < strings.Index(agent, "NT 6.0") {
			os += " Vista"
		} else if -1 < strings.Index(agent, "NT 6.1") {
			os += " 7"
		} else if -1 < strings.Index(agent, "NT 8") {
			os += " 8"
		} else if -1 < strings.Index(agent, "NT 10") {
			os += " 10"
		}

		return os
	}

	if -1 < strings.Index(agent, "iPhone") {
		return "iPhone"
	}

	if -1 < strings.Index(agent, "iPad") {
		return "iPad"
	}

	if -1 < strings.Index(agent, "Android") {
		return "Android"
	}

	if -1 < strings.Index(agent, "Mac") {
		return "Mac"
	}

	if -1 < strings.Index(agent, "Linux") {
		return "Linux"
	}

	if -1 < strings.Index(agent, "Unix") {
		return "Unix"
	}

	if -1 < strings.Index(agent, "FreeBSD") {
		return "FreeBSD"
	}

	if -1 < strings.Index(agent, "BSD") {
		return "BSD"
	}

	if -1 < strings.Index(agent, "IBM") {
		return "IBM"
	}

	if -1 < strings.Index(agent, "SunOS") {
		return "Solaris"
	}

	return "unknown"
}
