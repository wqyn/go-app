package main

import (
	"fmt"
	"reflect"
)

type Do struct {
	A string
}

func main() {

	_value := Do{A: "520"}

	_type := reflect.TypeOf(_value)
	_val := reflect.ValueOf(_value)

	fmt.Println(
		"", _type, "\r\n",
		"具体分类：", _type.Kind(), "\r\n",
		"自身类型名称： ", _type.Name(), "\r\n",
		"类型字符串标示: ", _type.String(), "\r\n",
		"返回类型的包路径 : ", _type.PkgPath(), "\r\n",
		"该类型值占用字节 :", _type.Size(), "\r\n",
	)

	fmt.Println(
		"返回T的切片类型: ", reflect.SliceOf(_type), "\r\n",
		"返回T的指针类型 ： ", reflect.PtrTo(_type), "\r\n",
	)

	fmt.Print(
		_val,
		_val.IsValid(),
		reflect.Zero(_type),
	)
}
