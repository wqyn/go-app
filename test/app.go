package main

import (
	"core/base"
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"strings"
	"time"
)

/****/
func init() {

}

type D map[string]interface{}

func (d D) A() interface{} {
	return d["A"]
}

func main() {

	d := D{"A": 1000000}

	fmt.Print("\r\n Test Value :", d.A(), "\r\n")

	var app = base.App

	app.Router().Get("/", func(w http.ResponseWriter, r *http.Request) string {

		//fmt.Fprint(w, "Hello, Router!!")

		return "Hello,World!!"
	})

	app.Router().Any("Get|Post", "/user/:id/:age", func(w http.ResponseWriter, r *http.Request) string {

		//tpl := template.New("A")
		//tpl.Parse("\r\n<!DOCTYPE HTML><html><a href='/user/100/100?'>Go TO</a></html>\r\n")
		//tpl.Execute(w, nil)

		fmt.Print("\r\n",
			app.Request().Host(), "\r\n",
			app.Request().Port(), "\r\n",
			app.Request().Url(), "\r\n",
			app.Request().RawUrl(), "\r\n",
			app.Request().Domain(), "\r\n",
			app.Request().Ext(), "\r\n",
			app.Request().Protocol(), "\r\n",
		)
		arrString := []string{"Boo", "Cpp", "8", "36", "5", "Akk", "Zpp", "1", "3", "2"}
		sort.Strings(arrString)

		fmt.Fprint(w, "Method :", r.Method, "\r\n",
			" Path : ", r.URL.Path, "\r\n",
			" RawPath : ", r.URL.RawPath, "\r\n",
			" RawQuery : ", r.URL.RawQuery, "\r\n",
			" Host : ", r.Host, "\r\n",
			" Proto : ", r.Proto, "\r\n",
			" RemoteAddr : ", r.RemoteAddr, "\r\n",
			" RequestURI : ", r.RequestURI, "\r\n",
			" Body :", r.Body, "\r\n",
			" Header :", r.Header, "\r\n",
			" PostForm", r.PostForm, "\r\n",
			" Time :", time.Now().Second(), "\r\n",
			" Ext :", app.Request().Ext(), "\r\n",
			" Port :", app.Request().Port(), "\r\n",
			" Domain :", app.Request().Domain(), "\r\n",
			"\r\n", "================Header==============", "\r\n",
			" X-Real-Ip :", r.Header["X-Real-Ip"], "\r\n",
			" X-Real-Ip :", r.Header.Get("X-Real-Ip"), "\r\n",
			" X-Forwarded-For :", r.Header["X-Forwarded-For"], "\r\n",
			" X-Forwarded-For :", r.Header.Get("X-Forwarded-For"), "\r\n",
			"X-Real-Ip : ", r.Header.Get("X-Real-Ip"), "\r\n",
			"Ip : ", app.Request().Ip(), "\r\n",
			r.Header.Get("A") == "", "\r\n",
			"Curr Time ", app.Request().Time(), "\r\n",
			"Referer ", app.Request().Referer(), "\r\n",
			"Query ", app.Request().Query(), "\r\n",

			"Accpet Get :", app.Request().Get().Accept("to", "fo"), "\r\n",
			"Excpet Get :", app.Request().Get().Except("to", "fo"), "\r\n",

			"GET ", app.Request().Get(), "\r\n",
			"ASC Sort Get: ", app.Request().Get().Asc(), "\r\n",
			"DESC Sort Get: ", app.Request().Get().Desc(), "\r\n",
			"GET Size: ", app.Request().Get().Size(), "\r\n",
			"GET Keys: ", app.Request().Get().Keys(), "\r\n",
			"Data :", app.Request().Data(), "\r\n",
			"Body :", r.Body, "\r\n",
			"PostForm :", app.Request().Post(), "\r\n",
			"Cookie : ", app.Request().Cookie(), "\r\n",
			"FileInfo:", app.Request().File(), "\r\n",
			"Agent :", app.Request().UserAgent(), "\r\n",
			"OS :", app.Request().Os(), "\r\n",
			"Browser :", app.Request().Browser(), "\r\n",
			"IsUpload : ", app.Request().IsUpload(), "\r\n",
			"IsMobile : ", app.Request().IsMobile(), "\r\n",

			"Strings 排序:", arrString, "\r\n",
		)

		var d bool

		fmt.Print("bool", d)

		ip := "255.255.255.255,0.0.0.0,192.168.10.10, 192.168.10.11, 192.168.10.12"

		fmt.Fprint(w, []uint8(ip), string([]int32(ip)[2+strings.LastIndex(ip, ","):]))
		return "201811!!"

	}).Pattern("id", "\\d+").Pattern("age", "\\d+")

	app.Router().Any("GET|POST", "/set/:id", func(w http.ResponseWriter, r *http.Request) string {

		form := "<!DOCTYPE HTML><html><head><meta charset='utf-8'><title>Test Upload</title></head>"
		form += "<body><form action='/user/100/10?fo=100&to=900' method='post' EncType='multipart/form-data'>"
		form += "<input name='file[]' type='file'> "
		form += "<input name='file[]' type='file'>"
		form += "<input name='img' type='file'>"
		form += "<input name='poo[A]' type='file'>"
		form += "<input name='poo[B]' type='file'>"
		form += "<input type='submit' value='SUBMIT'>"
		form += "</form></body></html>"

		tpl := template.New("a")
		tpl.Parse(form)

		tpl.Execute(w, nil)
		fmt.Fprint(w, app.Request().File())
		return "{}"
	}).Pattern("id", "\\d+")

	app.Router().Bye(func(writer http.ResponseWriter, request *http.Request) string {
		return "404 - Not Found!!"
	}, "GET")

	//for _key, _routes := range app.Router().GetRoutes(){
	//	for _, _route := range _routes{
	//		fmt.Print(_key, _route, "\r\n")
	//	}
	//}

	app.Serve("192.168.10.179", "8080")
}
