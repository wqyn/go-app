package main

import (
	"fmt"
	"core/routing"
	"net/http"
)

func main() {

	defer func() {

		//捕获异常
		if err := recover(); nil != err {
			fmt.Print(err)
		}

	}()


	var r  = routing.NewRouter()

	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {

	}).Domain("baidu.com").Port(90, 80)

	for n, routes := range r.GetRoutes() {
		for _, route := range routes {
			fmt.Print(n, " ", route, "\r\n")
		}
	}
}
