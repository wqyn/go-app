package main

import (
	"./core"
	"fmt"
	"html/template"
	"net/http"
	"os"
)

func contro(c core.ControllerInterface) {
	c.Get("Method Get" + "\r\n")
}

var s = &d

var d = 100

func main() {

	*s = 200
	str, _ := os.Getwd()
	//dir,_ := http.Dir(".")
	//_ , _ := os.Getwd()

	fmt.Print(*s, str, "\r\n")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		fmt.Print("Request URL: ", r.URL.Path, "\r\n")

		var path string = "/home/deepin/Projects/go/bubble/src"
		var file1 string = path + "/base/view/index.html"
		var file2 string = path + "/base/view/layout.html"

		//if _, err := os.Stat(file); err != nil {
		//	if os.IsNotExist(err) {
		//		fmt.Fprint(w, err)
		//	}
		//}

		tpl, _ := template.ParseFiles(file1, file2)

		data := struct {
			Number int
			Arr    []int
			Layout string
		}{
			100,
			[]int{10, 20, 30},
			"Layout.html",
		}

		//tpl.Funcs()
		tpl.ExecuteTemplate(w, "index.html", data)

		//if err := tpl.Execute(w, data); err != nil {
		//
		//	fmt.Fprint(w, err)
		//}
	})

	http.ListenAndServe("127.0.0.1:3000", nil)
	server := http.Server{
		Addr:    "127.0.0.1:3000",
		Handler: nil,
	}

	server.ListenAndServe()

	defer func() {
		fmt.Print("Run Now!!")
	}()
}
